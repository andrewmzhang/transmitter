#parse through a list from a folder or album, and put everything in track order

import os
import re
from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3
import mutagen.id3

def trackOrder(inputlist):
    newlist = []
    for element in inputlist:
        newlist.append(element)
    for element in inputlist:
        audio = MP3(element, ID3=EasyID3)
        tracknumber = (int(audio['tracknumber'][0])) - 1
        newlist[tracknumber] = element
    return newlist
