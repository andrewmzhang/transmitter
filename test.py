
import os
import subprocess
play_stereo = True
music_pipe_r,music_pipe_w = os.pipe()
with open(os.devnull, "w") as dev_null:
	global fm_process
	fm_process = subprocess.Popen(["/root/pifm","-","103.3","44100", "stereo" if play_stereo else "mono"], stdin=music_pipe_r, stdout=dev_null)
	subprocess.call(["ffmpeg","-i","sound.wav","-f","s16le","-acodec","pcm_s16le","-ac", "2" if play_stereo else "1" ,"-ar","44100","-"],stdout=music_pipe_w, stderr=dev_null)
