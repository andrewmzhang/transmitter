# returns file_list, which contains all the mp3 file locations within the selected folder.
#PLEASE improve the printing of the album titles, it looks ugly right now
import os
import re
def giveAlbum(fileFolder):  #asks user to choose a folder, returns the filelocations
    file_list = []
    albums = []
    choose = 0;
    pal = [] #barebones version of album

    for albumsname in os.listdir(fileFolder):
        if albumsname != 'desktop.ini':
            albums.append(albumsname)
            pal.append(albumsname.lower().replace(" ",""))
    print albums


    while(choose == 0):

        albumSelect = raw_input("Which album do you wish to play? ").lower().replace(" ","")
        if albumSelect in pal:
            choose = 1
            albumSelect = albums[pal.index(albumSelect)]
        else:
            print 'please try again'

    for root, folders, files in os.walk("fileFolder" + albumSelect):
        folders.sort()
        files.sort()
        for filename in files:
            if re.search(".(aac|mp3|wav|flac|m4a|pls|m3u)$", filename) != None:
                file_list.append(os.path.join(root, filename))
    return file_list
