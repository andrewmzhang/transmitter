#returns all the files within the location specified
import os
import re

def giveAll(fileFolder):
    file_list = []
    for root, folders, files in os.walk(fileFolder):
        folders.sort()
        files.sort()
        for filename in files:
            if re.search(".(aac|mp3|wav|flac|m4a|pls|m3u)$", filename) != None:
                file_list.append(os.path.join(root, filename))
    return file_list

